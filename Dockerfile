# Image: jstubbs/coe332-f18
# Build with: docker build -t jstubbs/coe332-f18 .
# Start with: docker run -p 5000:5000 jstubbs/coe332-f18

from python:3.7

RUN apt-get update && apt-get install -y python3-tk
COPY ./requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

COPY data /data
COPY api /api

ENTRYPOINT ["python"]

CMD ["/api/app.py"]


