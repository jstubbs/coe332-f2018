
def get_data(start=None, end=None):
    """Retuns a subset of the spots dataset; 
    start and stop, when supplied, should be the first and last years to include."""
    result = []
    with open('/data/sunspots.csv', 'r') as f:
        for idx, l in enumerate(f.readlines()):
            year, spots = l.split(',')
            result.append({'id': idx, 'year': year.strip(), 'spots': int(spots.strip())})
    if not start:
        start = result[0]['year']
    if not end:
        end = result[len(result) -1]['year']
    return [x for x in result if x.get('year') >= start and x.get('year') <= end]



