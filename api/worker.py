import time
from jobs import q, update_job_status, IN_PROGRESS, COMPLETE_STATUS

@q.worker
def execute_job(jid):
    """
    Function to execute a job. The HotQueue `q` object runs this function for each item placed on the queue, which
    should be a job id.

    :param job_key:
    :return:
    """
    # first, update status of job --
    update_job_status(jid, IN_PROGRESS)

    # todo -- replace with real job.
    time.sleep(15)
    update_job_status(jid, COMPLETE_STATUS)


execute_job()