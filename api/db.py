import os
import uuid

import matplotlib
# cf. https://stackoverflow.com/questions/37604289/tkinter-tclerror-no-display-name-and-no-display-environment-variable
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import redis
import rq

REDIS_IP = os.environ.get('REDIS_IP', '172.17.0.1')
try:
    REDIS_PORT = int(os.environ.get('REDIS_PORT'))
except:
    REDIS_PORT = 6379

DATA_DB = 0
QUEUE_DB = 1

SUBMITTED_STATUS = 'submitted'
COMPLETE_STATUS = 'complete'

redis_db = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=DATA_DB)

def get_by_id(id):
    """Returns the metadata for a single job object."""
    id, status, start, end = redis_db.hmget(id, 'id', 'status', 'start', 'end')
    return {"id": id.decode('utf-8'),
                   "status": status.decode('utf-8'),
                   "start": start.decode('utf-8'),
                   "end": end.decode('utf-8'),
            }
def delete_by_id(id):
    """Delete a single job."""
    redis_db.delete(id)

def get_all():
    """Returns all jobs in the system."""
    result = []
    for id in redis_db.keys():
        result.append(get_by_id(id))
    return result

def generate_uuid():
    """Generate a uuid for a job."""
    return str(uuid.uuid4())

def add_job(start, end):
    """Add a new job to the db and queue the job to be worked on."""
    uuid = generate_uuid()
    job = {'id': uuid,
           'status': SUBMITTED_STATUS,
           'start': start,
           'end': end,
           }
    redis_db.hmset(uuid, job)
    queue_job(uuid)
    return job

def finalize_job(job_id, file_path):
    """Update the job in the db with status and plot once worker has completed it."""
    job = get_by_id(job_id)
    job['status'] = COMPLETE_STATUS
    job['plot'] = open(file_path, 'rb').read()
    redis_db.hmset(job_id, job)

def get_job_plot(job_id):
    """Returns the plot, as binary data, associated with the job"""
    job = get_by_id(job_id)
    if not job['status'] == COMPLETE_STATUS:
        return True, "job not complete."
    return False, redis_db.hmget(job_id, 'plot')

def queue_job(job_id):
    """Add a new job to the task queue."""
    conn = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=QUEUE_DB)
    q = rq.Queue(connection=conn)
    q.enqueue('api.db.execute_job', job_id)

def execute_job(job_id):
    """Execute the job. This is the callable that is queued and worked on asynchronously."""
    job = get_by_id(job_id)
    from api.data import get_data
    points = get_data(job['start'], job['end'])
    years = [int(p['year']) for p in points]
    spots = [p['spots'] for p in points]
    plt.scatter(years, spots)
    tmp_file = '/tmp/{}.png'.format(job_id)
    plt.savefig(tmp_file, dpi=150)
    finalize_job(job_id, tmp_file)
