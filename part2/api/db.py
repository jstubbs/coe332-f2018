import uuid
import redis

SUBMITTED_STATUS = 'submitted'

redis_db = redis.StrictRedis(host="172.17.0.1", port=6379, db=0)

def get_all():
    result = []
    for x in redis_db.keys():
        id, status, start, stop = redis_db.hmget(x, 'id', 'status', 'start', 'stop')
        result.append({"id": id.decode('utf-8'),
                       "status": status.decode('utf-8'),
                       "start": start.decode('utf-8'),
                       "stop": stop.decode('utf-8'),
                       })
    return result

def get_by_id(id):
    return {id: redis_db.get(id)}

def add_object(id, obj):
    redis_db.set(id, obj)

def generate_uuid():
    return str(uuid.uuid4())

def add_job(start, stop):
    uuid = generate_uuid()
    job = {'id': uuid,
           'status': SUBMITTED_STATUS,
           'start': start,
           'stop': stop,
           }
    redis_db.hmset(uuid, job)
    return job

