import json
import os

from flask import Flask, request

import db

app = Flask(__name__)

# path to the data file
DATA_FILE = os.environ.get('DATA_FILE', '/data/sunspots.csv')

def get_data(start=None, end=None):
    """Retuns a subset of the spots dataset; 
    start and stop, when supplied, should be the first and last years to include."""
    result = []
    with open(DATA_FILE, 'r') as f:
        for idx, l in enumerate(f.readlines()):
            year, spots = l.split(',')
            result.append({'id': idx, 'year': int(year.strip()), 'spots': int(spots.strip())})
    if not start:
        start = result[0]['year']
    if not end:
        end = result[len(result) -1]['year']
    return [x for x in result if x.get('year') >= start and x.get('year') <= end]

def get_start_end():
    """Helper function to return the start and end parameters from the request, supplying defaults if not provided."""
    start = int(request.args.get('start', 0))
    end = int(request.args.get('end', 0))
    return start, end

def get_paged_data(limit=None, offset=None):
    data = get_data()
    if not limit:
        limit = len(data)
    if limit < 0:
        limit = 0
    if not offset:
        offset = 0
    return data[offset: limit + offset]

def do_paging(data):
    """Helper function to retrieve and apply limit and offset parameters to a list, data."""
    try:
        limit = int(request.args.get('limit', len(data)))
        offset = int(request.args.get('offset', 0))
    except ValueError:
        return json.dumps({'status': "Error", 'message': 'limit and offset, if provided, must be integers.'})
    return json.dumps(get_paged_data(limit, offset))

@app.route('/spots', methods=['GET'])
def spots():
    """Returns the spots data, applying start and end years and paging, if supplied in the request."""
    try:
        start, end = get_start_end()
    except (TypeError, ValueError):
        return json.dumps({'status': "Error", 'message': 'start and end, if provided, must be integers.'})
    if start or end:
        return json.dumps(get_data(start, end))
    return do_paging(get_data())

@app.route('/spots/<id>', methods=['GET'])
def spots_for_id(id):
    """Return the spots data point for a single row id."""
    try:
        int(id)
    except ValueError:
        return json.dumps({'status': "Error", 'message': 'id must be an integer.'})
    result = [x for x in get_data() if x.get('id') == int(id)]
    return json.dumps(result)

@app.route('/spots/year/<year>', methods=['GET'])
def spots_for_year(year):
    """Return the spots data point for a single year."""
    try:
        int(year)
    except:
        return json.dumps({'status': "Error", 'message': 'year must be an integer.'})
    result = [x for x in get_data() if x.get('year') == int(year)]
    return json.dumps(result)

def validate_job():
    try:
        job = request.get_json(force=True)
    except Exception as e:
        return True, json.dumps({'status': "Error", 'message': 'Invalid JSON: {}.'.format(e)})
    start = job.get('start')
    try:
        job['start'] = int(start)
    except:
        return True, json.dumps({'status': "Error", 'message': 'start parameter must be an integer.'})
    stop = job.get('stop')
    try:
        job['stop'] = int(stop)
    except:
        return True, json.dumps({'status': "Error", 'message': 'stop parameter must be an integer.'})
    if job['start'] > job['stop']:
        return True, json.dumps({'status': "Error", 'message': 'start must be less than or equal to stop.'})
    return False, job

@app.route('/jobs', methods=['GET', 'POST'])
def jobs():
    if request.method == 'POST':
        error, job = validate_job()
        if error:
            return job
        return json.dumps(db.add_job(job['start'], job['stop']))
    else:
        data = db.get_all()
        return do_paging(data)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')