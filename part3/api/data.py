import os

# path to the data file
DATA_FILE = os.environ.get('DATA_FILE', '/data/sunspots.csv')

def get_data(start=None, end=None):
    """Retuns a subset of the spots dataset; 
    start and stop, when supplied, should be the first and last years to include."""
    result = []
    with open(DATA_FILE, 'r') as f:
        for idx, l in enumerate(f.readlines()):
            year, spots = l.split(',')
            result.append({'id': idx, 'year': int(year.strip()), 'spots': int(spots.strip())})
    if not start:
        start = result[0]['year']
    if not end:
        end = result[len(result) -1]['year']
    return [x for x in result if x.get('year') >= start and x.get('year') <= end]

def get_paged_data(limit=None, offset=None):
    data = get_data()
    if not limit:
        limit = len(data)
    if limit < 0:
        limit = 0
    if not offset:
        offset = 0
    return data[offset: limit + offset]

