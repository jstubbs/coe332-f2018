import json
import io
from flask import Flask, send_file, request

import data
import db

app = Flask(__name__)

def get_start_end():
    """Helper function to return the start and end parameters from the request, supplying defaults if not provided."""
    start = int(request.args.get('start', 0))
    end = int(request.args.get('end', 0))
    return start, end

def do_paging(data):
    """Helper function to retrieve and apply limit and offset parameters to a list, data."""
    try:
        limit = int(request.args.get('limit', len(data)))
        offset = int(request.args.get('offset', 0))
    except ValueError:
        return json.dumps({'status': "Error", 'message': 'limit and offset, if provided, must be integers.'})
    return json.dumps(data.get_paged_data(limit, offset))

@app.route('/spots', methods=['GET'])
def spots():
    """Returns the spots data, applying start and end years and paging, if supplied in the request."""
    try:
        start, end = get_start_end()
    except (TypeError, ValueError):
        return json.dumps({'status': "Error", 'message': 'start and end, if provided, must be integers.'})
    if start or end:
        return json.dumps(data.get_data(start, end))
    return do_paging(data.get_data())

@app.route('/spots/<id>', methods=['GET'])
def spots_for_id(id):
    """Return the spots data point for a single row id."""
    try:
        int(id)
    except ValueError:
        return json.dumps({'status': "Error", 'message': 'id must be an integer.'})
    result = [x for x in data.get_data() if x.get('id') == int(id)]
    return json.dumps(result)

@app.route('/spots/year/<year>', methods=['GET'])
def spots_for_year(year):
    """Return the spots data point for a single year."""
    try:
        int(year)
    except:
        return json.dumps({'status': "Error", 'message': 'year must be an integer.'})
    result = [x for x in data.get_data() if x.get('year') == int(year)]
    return json.dumps(result)

def validate_job():
    """Check that a job request is valid; start and end must be be supplied and be integers."""
    try:
        job = request.get_json(force=True)
    except Exception as e:
        return True, json.dumps({'status': "Error", 'message': 'Invalid JSON: {}.'.format(e)})
    start = job.get('start')
    try:
        job['start'] = int(start)
    except:
        return True, json.dumps({'status': "Error", 'message': 'start parameter must be an integer.'})
    end = job.get('end')
    try:
        job['end'] = int(end)
    except:
        return True, json.dumps({'status': "Error", 'message': 'stop parameter must be an integer.'})
    if job['start'] > job['end']:
        return True, json.dumps({'status': "Error", 'message': 'start must be less than or equal to end.'})
    return False, job

@app.route('/jobs', methods=['GET', 'POST'])
def jobs():
    """List all jobs and create new jobs."""
    if request.method == 'POST':
        error, job = validate_job()
        if error:
            return job
        return json.dumps(db.add_job(job['start'], job['end']))
    else:
        data = db.get_all()
        return do_paging(data)

@app.route('/jobs/<job_id>', methods=['GET', 'DELETE'])
def job(job_id):
    """List and delete a job by job id."""
    if request.method == 'GET':
        return json.dumps(db.get_by_id(job_id))
    else:
        db.delete_by_id(job_id)
        return json.dumps({'status': 'Success', 'message': 'job {} deleted.'.format(job_id)})

@app.route('/jobs/<job_id>/plot', methods=['GET'])
def job_plot(job_id):
    """Returns the plot produced by a job as a binary png file attachment to the response."""
    error, plot = db.get_job_plot(job_id)
    if error:
       return json.dumps({'status': "Error", 'message': plot})
    return send_file(io.BytesIO(plot[0]),
                     mimetype='image/png',
                     as_attachment=True,
                     attachment_filename='{}.png'.format(job_id))

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')