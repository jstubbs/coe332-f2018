#!/bin/bash

export BASE=http://localhost:5000


curl $BASE/spots

curl $BASE/spots/17

curl $BASE/spots/year/1804

curl "$BASE/spots?start=1788&end=1800"


curl $BASE/jobs


curl $BASE/jobs/$JOB_ID

curl -X DELETE $BASE/jobs/$JOB_ID

curl -d '{"start": "a"}' $BASE/jobs

curl -d '{"start": 10}' $BASE/jobs

curl -d '{"start": 10, "end": 20}' $BASE/jobs

curl $BASE/jobs/$JOB_ID/plot


