import os
import requests

BASE_URL = os.environ.get('BASE_URL', 'http://0.0.0.0:5000')


def get_json_url(url):
    rsp = requests.get(url)
    rsp.raise_for_status()
    return rsp.json()

def test_get_spots_json():
    url = '{}/spots'.format(BASE_URL)
    get_json_url(url)

def test_get_spots_len():
    url = '{}/spots'.format(BASE_URL)
    data = get_json_url(url)
    assert len(data) == 100

def test_get_data_dict_keys():
    url = '{}/spots'.format(BASE_URL)
    data = get_json_url(url)
    for d in data:
        assert len(d.keys()) == 3

def test_get_data_dict_keys_type():
    url = '{}/spots'.format(BASE_URL)
    data = get_json_url(url)
    for d in data:
        assert type(d.get('id')) == int
        assert type(d.get('year')) == int
        assert type(d.get('spots')) == int

def test_get_data_0():
    url = '{}/spots'.format(BASE_URL)
    data = get_json_url(url)
    assert data[0].get('id') == 0
    assert data[0].get('year') == 1770
    assert data[0].get('spots') == 101

def test_get_data_last():
    url = '{}/spots'.format(BASE_URL)
    data = get_json_url(url)
    d = data[len(data) - 1]
    assert d.get('id') == 99
    assert d.get('year') == 1869
    assert d.get('spots') == 74

def test_get_data_start_no_end():
    url = '{}/spots?start=1867'.format(BASE_URL)
    data = get_json_url(url)
    assert len(data) == 3

def test_get_data_end_no_start():
    url = '{}/spots?end=1771'.format(BASE_URL)
    data = get_json_url(url)
    assert len(data) == 2

def test_get_data_start_and_end():
    url = '{}/spots?start=1771&end=1771'.format(BASE_URL)
    data = get_json_url(url)
    assert len(data) == 1

def test_get_data_start_gt_end():
    url = '{}/spots?start=1772&end=1771'.format(BASE_URL)
    data = get_json_url(url)
    assert len(data) == 0

def test_get_data_small_start():
    url = '{}/spots?start=-1&end=1771'.format(BASE_URL)
    data = get_json_url(url)
    assert len(data) == 2

def test_get_data_large_end():
    url = '{}/spots?start=-1&end=5000'.format(BASE_URL)
    data = get_json_url(url)
    assert len(data) == 100

def test_get_data_limit():
    url = '{}/spots?limit=2'.format(BASE_URL)
    data = get_json_url(url)
    assert len(data) == 2

def test_get_data_offset():
    url = '{}/spots?offset=2'.format(BASE_URL)
    data = get_json_url(url)
    assert len(data) == 98

def test_get_data_limit_and_offset():
    url = '{}/spots?limit=2&offset=90'.format(BASE_URL)
    data = get_json_url(url)
    assert len(data) == 2

def test_get_data_large_limit():
    url = '{}/spots?limit=2000'.format(BASE_URL)
    data = get_json_url(url)
    assert len(data) == 100

def test_get_data_small_limit():
    url = '{}/spots?limit=-1'.format(BASE_URL)
    data = get_json_url(url)
    assert len(data) == 0
