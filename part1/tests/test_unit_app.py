import os

# make sure the current working directory is on the python path -
import sys
sys.path.append(os.getcwd())

from api import app

def test_get_data_list():
    assert type(app.get_data()) == list

def test_get_data_dict():
    data = app.get_data()
    for d in data:
        assert type(d) == dict

def test_get_data_dict_keys():
    data = app.get_data()
    for d in data:
        assert len(d.keys()) == 3

def test_get_data_dict_keys_type():
    data = app.get_data()
    for d in data:
        assert type(d.get('id')) == int
        assert type(d.get('year')) == int
        assert type(d.get('spots')) == int

def test_get_data_len():
    data = app.get_data()
    assert len(data) == 100

def test_get_data_0():
    data = app.get_data()
    assert data[0].get('id') == 0
    assert data[0].get('year') == 1770
    assert data[0].get('spots') == 101

def test_get_data_last():
    data = app.get_data()
    d = data[len(data) - 1]
    assert d.get('id') == 99
    assert d.get('year') == 1869
    assert d.get('spots') == 74

def test_get_data_start_no_end():
    data = app.get_data(start=1867)
    assert len(data) == 3

def test_get_data_end_no_start():
    data = app.get_data(end=1771)
    assert len(data) == 2

def test_get_data_start_and_end():
    data = app.get_data(start=1771, end=1771)
    assert len(data) == 1

def test_get_data_start_gt_end():
    data = app.get_data(start=1772, end=1771)
    assert len(data) == 0

def test_get_data_small_start():
    data = app.get_data(start=-1, end=1771)
    assert len(data) == 2

def test_get_data_large_end():
    data = app.get_data(start=-1, end=5000)
    assert len(data) == 100

def test_get_data_limit():
    data = app.get_paged_data(limit=2)
    assert len(data) == 2

def test_get_data_offset():
    data = app.get_paged_data(offset=2)
    assert len(data) == 98

def test_get_data_limit_and_offset():
    data = app.get_paged_data(limit=2, offset=90)
    assert len(data) == 2

def test_get_data_large_limit():
    data = app.get_paged_data(limit=2000)
    assert len(data) == 100

def test_get_data_small_limit():
    data = app.get_paged_data(limit=-1)
    assert len(data) == 0
