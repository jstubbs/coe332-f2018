Part 1
======

1) create a virtualenv with the following command:
   $ virtualenv -p /usr/bin/python3 coef18_part1

2) activate the virtualenv:
   $ source coef18_part1/bin/activate

3) install the requirements:
   $ pip install -r requirements.txt

4) start the python application:
   $ python api/app.py

(optional)
5) run the unit tests:
   $ py.test tests/test_unit_app.py

6) run the integration tests (requires application to be running):
   $ py.test tests/test_integration_app.py